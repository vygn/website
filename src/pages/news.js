import React from 'react';
import { Link } from 'gatsby';
import { graphql } from 'gatsby';

import Header from '../components/header';
import SEO from '../components/seo';
import Footer from '../components/footer';

const SecondPage = ({ data }) => ( <
    div className = 'mainbody' >
    <
    SEO title = 'News' / >
    <
    Header / >
    <
    div className = 'site-content' >
    <
    div classname = 'news-list' >
    <
    div className = 'blog-heading' >
    <
    h1 class = 'underline-small' > Latest News < /h1> <
    /div> <
    /div>



    <
    /div> <
    Footer / >
    <
    /div>
);

export const pageQuery = graphql `
{  
    "data": {
      "allToot": {
        "edges": [
          {
            "node": {
              "id": "0b081580-42cf-590e-80d9-bfc683545a91",
              "url": "https://mas.to/users/fossnss/statuses/107127045384382348/activity",
              "content": "The camp registration deadline is extended to 5 November 2021. Please visit <a href=\"https://camp.fsci.in\" rel=\"nofollow noopener noreferrer\" target=\"_blank\">camp.fsci.in</a> to register.",
              "reblogs_count": 0,
              "favourites_count": 0,
              "account": {
                "username": "fossnss"
              }
            }
          },
          {
            "node": {
              "id": "35c08d6e-6b59-51d0-9e9e-e8f86d804d32",
              "url": "https://mas.to/@fossnss/106844543189379664",
              "content": "<p>We had a wonderful session from <br />@joel_wasserman_<br /> today on &apos;Journey with Open Source&apos;.<br />Extremely happy to hear the story and gain insights on <br />@flossbank<br />  and <a href=\"http://theteacherfund.com\" rel=\"nofollow noopener noreferrer\" target=\"_blank\"><span class=\"invisible\">http://</span><span class=\"\">theteacherfund.com</span><span class=\"invisible\"></span></a> from the creator itself :)</p><p>[Oct 10, 2020]</p>",
              "reblogs_count": 0,
              "favourites_count": 0,
              "account": {
                "username": "fossnss"
              }
            }
          },
          {
            "node": {
              "id": "edc0a461-8862-5955-9e5b-d64e99e53920",
              "url": "https://mas.to/@fossnss/106844540095596483",
              "content": "<p>It&apos;s not always the code. A good part of building relations in communities/self is good communication!<br />A big thanks to  @1nuritzi for taking the time from her busy schedule for this awesome session. <br />We really had a good time and especially the Q&amp;A 🎉</p><p>[Oct 14, 2020]</p>",
              "reblogs_count": 0,
              "favourites_count": 0,
              "account": {
                "username": "fossnss"
              }
            }
          },
          {
            "node": {
              "id": "69345c00-243a-5e96-b78b-7272b0e10c23",
              "url": "https://mas.to/users/fossnss/statuses/106844528951557207/activity",
              "content": "<p><span class=\"h-card\"><a href=\"https://mastodon.social/@sonnie\" class=\"u-url mention\" rel=\"nofollow noopener noreferrer\" target=\"_blank\">@<span>sonnie</span></a></span> sharing his <a href=\"https://social.masto.host/tags/Outreachy\" class=\"mention hashtag\" rel=\"nofollow noopener noreferrer\" target=\"_blank\">#<span>Outreachy</span></a> experience with work with <a href=\"https://social.masto.host/tags/debian\" class=\"mention hashtag\" rel=\"nofollow noopener noreferrer\" target=\"_blank\">#<span>debian</span></a> </p><p>My internships with the Debian js-team has been so eventful and gracefully intense that hardly did I realize 13 weeks are already gone :) . This, of course does not in any way breach the intensity of my involvement with Debian, but rather means I could now take up more general packaging tasks than just those specific to my Internship projects.</p><p><a href=\"https://sonnie.hashnode.dev/outreachy-internship-wrap-up\" rel=\"nofollow noopener noreferrer\" target=\"_blank\"><span class=\"invisible\">https://</span><span class=\"ellipsis\">sonnie.hashnode.dev/outreachy-</span><span class=\"invisible\">internship-wrap-up</span></a></p><p><span class=\"h-card\"><a href=\"https://mastodon.technology/@akshay\" class=\"u-url mention\" rel=\"nofollow noopener noreferrer\" target=\"_blank\">@<span>akshay</span></a></span> and me mentored this project. <a href=\"https://social.masto.host/tags/FreeSoftware\" class=\"mention hashtag\" rel=\"nofollow noopener noreferrer\" target=\"_blank\">#<span>FreeSoftware</span></a></p>",
              "reblogs_count": 0,
              "favourites_count": 0,
              "account": {
                "username": "fossnss"
              }
            }
          }
        ]
      }
    }
}
`;
export default SecondPage;